﻿namespace Draw
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ListCoord = new System.Windows.Forms.DataGridView();
            this.isBegin = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Insert = new System.Windows.Forms.DataGridViewImageColumn();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.Load = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.Info = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.countRows = new System.Windows.Forms.PictureBox();
            this.DrawArea = new System.Windows.Forms.PictureBox();
            this.showXY = new System.Windows.Forms.CheckBox();
            this.saveAsPic = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ListCoord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countRows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DrawArea)).BeginInit();
            this.SuspendLayout();
            // 
            // ListCoord
            // 
            this.ListCoord.AllowUserToAddRows = false;
            this.ListCoord.AllowUserToDeleteRows = false;
            this.ListCoord.AllowUserToResizeColumns = false;
            this.ListCoord.AllowUserToResizeRows = false;
            this.ListCoord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.ListCoord.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.isBegin,
            this.Column1,
            this.Column2,
            this.Insert,
            this.Delete});
            this.ListCoord.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ListCoord.Location = new System.Drawing.Point(12, 12);
            this.ListCoord.MultiSelect = false;
            this.ListCoord.Name = "ListCoord";
            this.ListCoord.RowHeadersVisible = false;
            this.ListCoord.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ListCoord.Size = new System.Drawing.Size(236, 479);
            this.ListCoord.TabIndex = 4;
            this.ListCoord.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.ListCoord_CellBeginEdit);
            this.ListCoord.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ListCoord_CellEndEdit);
            this.ListCoord.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ListCoord_CellEnter);
            this.ListCoord.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ListCoord_CellMouseUp);
            this.ListCoord.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ListCoord_CellValueChanged);
            this.ListCoord.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ListCoord_EditingControlShowing);
            this.ListCoord.SelectionChanged += new System.EventHandler(this.ListCoord_SelectionChanged);
            // 
            // isBegin
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = false;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.Window;
            this.isBegin.DefaultCellStyle = dataGridViewCellStyle1;
            this.isBegin.FalseValue = "false";
            this.isBegin.FillWeight = 55F;
            this.isBegin.HeaderText = "Початок";
            this.isBegin.IndeterminateValue = "false";
            this.isBegin.Name = "isBegin";
            this.isBegin.ReadOnly = true;
            this.isBegin.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.isBegin.TrueValue = "true";
            this.isBegin.Width = 55;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column1.FillWeight = 50F;
            this.Column1.HeaderText = "X";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column2.FillWeight = 50F;
            this.Column2.HeaderText = "Y";
            this.Column2.Name = "Column2";
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 50;
            // 
            // Insert
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle4.NullValue")));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.Window;
            this.Insert.DefaultCellStyle = dataGridViewCellStyle4;
            this.Insert.FillWeight = 30F;
            this.Insert.HeaderText = "";
            this.Insert.Image = global::Draw.Properties.Resources.add_icon;
            this.Insert.MinimumWidth = 30;
            this.Insert.Name = "Insert";
            this.Insert.ReadOnly = true;
            this.Insert.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Insert.Width = 30;
            // 
            // Delete
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle5.NullValue")));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.Window;
            this.Delete.DefaultCellStyle = dataGridViewCellStyle5;
            this.Delete.FillWeight = 30F;
            this.Delete.HeaderText = "";
            this.Delete.Image = global::Draw.Properties.Resources.delete;
            this.Delete.MinimumWidth = 30;
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Delete.Width = 30;
            // 
            // Load
            // 
            this.Load.ForeColor = System.Drawing.Color.DarkRed;
            this.Load.Location = new System.Drawing.Point(12, 497);
            this.Load.Name = "Load";
            this.Load.Size = new System.Drawing.Size(85, 23);
            this.Load.TabIndex = 5;
            this.Load.Text = "Завантажити";
            this.Load.UseVisualStyleBackColor = true;
            this.Load.Click += new System.EventHandler(this.Load_Click);
            // 
            // Save
            // 
            this.Save.ForeColor = System.Drawing.Color.DarkRed;
            this.Save.Location = new System.Drawing.Point(90, 497);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(68, 23);
            this.Save.TabIndex = 6;
            this.Save.Text = "Зберегти";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Clear
            // 
            this.Clear.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Clear.Location = new System.Drawing.Point(164, 497);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(75, 23);
            this.Clear.TabIndex = 8;
            this.Clear.Text = "Очистити";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Info
            // 
            this.Info.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Info.ForeColor = System.Drawing.Color.DarkRed;
            this.Info.Location = new System.Drawing.Point(922, 494);
            this.Info.Margin = new System.Windows.Forms.Padding(0);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(32, 30);
            this.Info.TabIndex = 9;
            this.Info.Text = "?";
            this.Info.UseVisualStyleBackColor = true;
            this.Info.Click += new System.EventHandler(this.Info_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.FillWeight = 30F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn1.Image")));
            this.dataGridViewImageColumn1.MinimumWidth = 30;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn1.Width = 30;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.FillWeight = 30F;
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = ((System.Drawing.Image)(resources.GetObject("dataGridViewImageColumn2.Image")));
            this.dataGridViewImageColumn2.MinimumWidth = 30;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn2.Width = 30;
            // 
            // countRows
            // 
            this.countRows.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.countRows.Image = global::Draw.Properties.Resources.Gray;
            this.countRows.Location = new System.Drawing.Point(567, 497);
            this.countRows.Name = "countRows";
            this.countRows.Size = new System.Drawing.Size(94, 23);
            this.countRows.TabIndex = 10;
            this.countRows.TabStop = false;
            // 
            // DrawArea
            // 
            this.DrawArea.Image = global::Draw.Properties.Resources.White;
            this.DrawArea.Location = new System.Drawing.Point(258, 15);
            this.DrawArea.Name = "DrawArea";
            this.DrawArea.Size = new System.Drawing.Size(693, 476);
            this.DrawArea.TabIndex = 3;
            this.DrawArea.TabStop = false;
            // 
            // showXY
            // 
            this.showXY.AutoSize = true;
            this.showXY.BackColor = System.Drawing.SystemColors.Control;
            this.showXY.ForeColor = System.Drawing.Color.IndianRed;
            this.showXY.Location = new System.Drawing.Point(245, 501);
            this.showXY.Name = "showXY";
            this.showXY.Size = new System.Drawing.Size(117, 17);
            this.showXY.TabIndex = 11;
            this.showXY.Text = "Координати точок";
            this.showXY.UseVisualStyleBackColor = false;
            this.showXY.CheckedChanged += new System.EventHandler(this.showXY_CheckedChanged);
            // 
            // saveAsPic
            // 
            this.saveAsPic.ForeColor = System.Drawing.Color.OliveDrab;
            this.saveAsPic.Image = global::Draw.Properties.Resources.saveScreenShot;
            this.saveAsPic.Location = new System.Drawing.Point(889, 494);
            this.saveAsPic.Name = "saveAsPic";
            this.saveAsPic.Size = new System.Drawing.Size(30, 30);
            this.saveAsPic.TabIndex = 12;
            this.saveAsPic.UseVisualStyleBackColor = true;
            this.saveAsPic.Click += new System.EventHandler(this.saveAsPic_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 530);
            this.Controls.Add(this.saveAsPic);
            this.Controls.Add(this.showXY);
            this.Controls.Add(this.countRows);
            this.Controls.Add(this.Info);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.Load);
            this.Controls.Add(this.ListCoord);
            this.Controls.Add(this.DrawArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Координатна площина";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ListCoord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countRows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DrawArea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox DrawArea;
        private System.Windows.Forms.DataGridView ListCoord;
        private System.Windows.Forms.Button Load;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Info;
        private System.Windows.Forms.PictureBox countRows;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isBegin;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewImageColumn Insert;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
        private System.Windows.Forms.CheckBox showXY;
        private System.Windows.Forms.Button saveAsPic;
    }
}

