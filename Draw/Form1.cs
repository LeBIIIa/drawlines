﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Drawing.Imaging;

namespace Draw
{
    [Serializable]
    public struct myPoint{
        public double x, y;
        public bool isBegin;
        public myPoint( double x, double y, bool isBegin)
        {
            this.x = x;
            this.y = y;
            this.isBegin = isBegin;
        }
    }

    public partial class Form1 : Form
    {
        private const int HLnumbers = -15;
        private const int HRnumbers = 16;
        private const int VLnumbers = -10;
        private const int VRnumbers = 11;

        private bool isChange = false;
        private bool canDraw = false;
        private string filename = null;

        private const int backing_zero = 22;
        private const float X_center = 340 + 11f;
        private const float Y_center = 237 + 6.5f;
        private const float X = 340 + 12f;
        private const float Y = 237 + 8.009f;

        private const int MAX_ROW = 128;

        public Form1()
        {
            InitializeComponent();

            ToolTip tip = new ToolTip();
            tip.SetToolTip(Load, "з диска(F3)");
            tip.SetToolTip(Save, "на диск(F2)");
            tip.SetToolTip(Clear, "таблицю і дошку(F8)");
            tip.SetToolTip(Info, "Допомога(F1)");
            tip.SetToolTip(saveAsPic, "Скриншот малюнку(S)");
            countRows.Location = new Point(DrawArea.Location.X + (DrawArea.Width - countRows.Width) / 2, countRows.Location.Y);

            initDataGrid();
            canDraw = true;
            DrawOnDesk(null);
        }

        private void initDataGrid()
        {
            Console.WriteLine("init datagrid");
            ListCoord.Rows.Clear();
            for (int i = 0; i < MAX_ROW; ++i)
            {
                ListCoord.Rows.Add();
            }
            ListCoord[0, 0].Value = true;
            ListCoord.CurrentCell = ListCoord[1, 0];
        }

        private void Draw_BackGround(Bitmap picture)
        {
            Console.WriteLine("Draw_BackGround");
            Graphics g;
            g = Graphics.FromImage((picture == null) ? DrawArea.Image : picture);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            
            Font drawFont = new Font("Arial", 3);
            Pen mypen = new Pen(Brushes.Gray);

            if ( picture != null )
            {
                drawFont = new Font("Arial", 10);
            }

            double width = DrawArea.Size.Width;
            double height = DrawArea.Size.Height;

            if ( picture != null )
            {
                width = picture.Width;
                height = picture.Height;
            }

            Point up = new Point(10, 0);
            Point down = new Point(0, (int)height);
            string s = "";
            //Horizontal number
            for ( int i = HLnumbers;i < HRnumbers;++i )
            {
                if ( i >= 0 )
                {
                    s += " ";
                }
                s += i.ToString();
                g.DrawString(s, drawFont, Brushes.Blue, up);
                up.X += backing_zero;
                s = "";
            }
            //Horizontal Lines
            up = new Point(22, 25);
            down = new Point((int)width - 12, 25);
            for (int i = VLnumbers; i < VRnumbers; ++i)
            {
                if (i == 0)
                {
                    g.DrawLine(new Pen(Brushes.Red), up, down);
                }
                else
                {
                    g.DrawLine(mypen, up, down);
                }
                up.Y += backing_zero;
                down.Y += backing_zero;
            }
            //Vertical number
            s = "";
            up = new Point(0, 17);
            for (int i = VRnumbers - 1; i >= VLnumbers; --i)
            {
                if (i >= 0)
                {
                    s += " ";
                }
                s += i.ToString();
                g.DrawString(s, drawFont, Brushes.Blue, up);
                up.Y += backing_zero;
                s = "";
            }
            //Vertical Lines
            up = new Point(22, 25);
            down = new Point(22, (int)height - 11);
            for (int i = HLnumbers; i < HRnumbers; ++i)
            {
                if (i == 0)
                {
                    g.DrawLine(new Pen(Brushes.Red), up, down);
                }
                else
                {
                    g.DrawLine(mypen, up, down);
                }
                up.X += backing_zero;
                down.X += backing_zero;
            }
            g.Dispose();

        }

        private List<myPoint> getList()
        {
            List<myPoint> points = new List<myPoint>();
            float x1, y1;
            bool flag;
            DataGridViewRowCollection rowCollection = ListCoord.Rows;

            for (int i = 0; i < rowCollection.Count; ++i)
            {
                object s1 = rowCollection[i].Cells["Column1"].Value;
                object s2 = rowCollection[i].Cells["Column2"].Value;
                object s3 = rowCollection[i].Cells["isBegin"].Value;
                if (s1 == null || s1.ToString().Equals("")) { break; }
                if (s2 == null || s2.ToString().Equals("")) { break; }
                x1 = float.Parse(s1.ToString());
                y1 = float.Parse(s2.ToString());
                flag = Convert.ToBoolean(s3);
                myPoint point = new myPoint(x1, y1, flag);
                points.Add(point);
            }
            return points;
        }

        private void DrawLines(Bitmap picture)
        {
            Console.WriteLine("DrawLines");
            Graphics g;
            g = Graphics.FromImage((picture == null) ? DrawArea.Image : picture);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            Pen mypen = new Pen(Color.LightSkyBlue, 2);
            ArrayList points = new ArrayList();

            Font drawFont = new Font("Arial", 3, FontStyle.Bold);

            if ( picture != null )
            {
                drawFont = new Font("Arial", 10, FontStyle.Bold);
            }

            int allPoints = 0;
            float x1 = 0, y1 = 0;
            bool flag;
            try
            {
                DataGridViewRowCollection rowCollection = ListCoord.Rows;

                for (int i = 0; i < rowCollection.Count; ++i)
                {
                    object s1 = rowCollection[i].Cells["Column1"].Value;
                    object s2 = rowCollection[i].Cells["Column2"].Value;
                    object s3 = rowCollection[i].Cells["isBegin"].Value;
                    if (s1 == null || s1.ToString().Equals("")) { break; }
                    if (s2 == null || s2.ToString().Equals("")) { break; }
                    x1 = float.Parse(s1.ToString());
                    y1 = float.Parse(s2.ToString());
                    flag = Convert.ToBoolean(s3);
                    if (i == 0) flag = false;
                    if (flag == true)
                    {
                        if (points.Count >= 2)
                        {
                            PointF[] ps = points.ToArray(typeof(PointF)) as PointF[];
                            g.DrawLines(mypen, ps);
                        }
                        allPoints += points.Count;
                        points.Clear();
                    }
                    PointF point = new PointF(X + x1 * backing_zero, Y - y1 * backing_zero);
                    points.Add(point);
                    isChange = true;
                }
                if (points.Count >= 2)
                {
                    PointF[] ps = points.ToArray(typeof(PointF)) as PointF[];
                    g.DrawLines(mypen, ps);
                }
                allPoints += points.Count;
                for (int i = 0; i < rowCollection.Count; ++i)
                {
                    object s1 = rowCollection[i].Cells["Column1"].Value;
                    object s2 = rowCollection[i].Cells["Column2"].Value;
                    if (s1 == null || s1.ToString().Equals("")) { break; }
                    if (s2 == null || s2.ToString().Equals("")) { break; }
                    x1 = float.Parse(s1.ToString());
                    y1 = float.Parse(s2.ToString());
                    g.FillRectangle(new SolidBrush(Color.Black), X_center + x1 * backing_zero, Y_center - y1 * backing_zero, 3, 3);
                    if (showXY.Checked)
                    {
                        string str = x1 + "," + y1;
                        g.DrawString(str, drawFont, new SolidBrush(Color.Black), X_center + x1 * backing_zero - 10, Y_center - y1 * backing_zero - 15);
                    }
                }
                if (picture == null)
                {
                    g = Graphics.FromImage(countRows.Image);
                    g.Clear(Color.LightGray);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    string s = allPoints + "/128 рядків";
                    drawFont = new Font("Arial", 8, FontStyle.Bold);
                    Brush drawBrush = Brushes.Indigo;
                    PointF p = new PointF((countRows.Width) / 2 - s.Length * 3, countRows.Height / 2 - 8);
                    g.DrawString(s, drawFont, drawBrush, p);
                    countRows.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                g.Dispose();
            }
        }

        private void clear_Table(Bitmap picture)
        {
            Console.WriteLine("Clear");
            Graphics g;
            g = Graphics.FromImage((picture == null) ? DrawArea.Image : picture);
            g.Clear(Color.White);
            g.Dispose();
            Draw_BackGround(picture);

        }

        private void DrawOnDesk(Bitmap picture)
        {
            clear_Table(picture);
            DrawLines(picture);
            if ( picture == null ) {
                DrawArea.Refresh();
            }
        }

        private void betterSave()
        {
            DialogResult result;
            result = MessageBox.Show("Зберегти координати малюнку?", "Підтверження", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (result == DialogResult.Yes)
            {
                Save.PerformClick();
            }
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ( e.KeyChar == '.' )
                e.KeyChar = ',';

            if (e.KeyChar == '-' && (sender as TextBox).Text.Length != 0)
                e.Handled = true;

            if (e.KeyChar == ',' && (sender as TextBox).Text.Length == 3)
                e.Handled = true;
            
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != ',') && (e.KeyChar != '-'))
                e.Handled = true;

            int len = 0;
            if (char.IsNumber(e.KeyChar))
            {
                len = (sender as TextBox).Text.Length;
                int i = (sender as TextBox).Text.IndexOf('-');
                len -= (i > -1) ? 1 : 0;
                i = (sender as TextBox).Text.IndexOf(',');
                len -= (i > -1) ? 1 : 0;

                if (len >= 3)
                    e.Handled = true;

                if ( len != 0 && len != 3 && e.KeyChar == ',')
                    e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
                e.Handled = true;
        }

        private void ListCoord_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if ( ListCoord.CurrentCell.ColumnIndex == 1 || ListCoord.CurrentCell.ColumnIndex == 2 ) {
                e.Control.KeyPress -= new KeyPressEventHandler(textBox_KeyPress);
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(textBox_KeyPress);
                }
            }
        }

        private void ListCoord_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if ( e.RowIndex == 0 && e.ColumnIndex == 0 )
                ListCoord[0, 0].Value = true;

            if ( e.ColumnIndex == 1 || e.ColumnIndex == 2 )
            {
                try
                {
                    double s = double.Parse(ListCoord[e.ColumnIndex, e.RowIndex].Value.ToString());
                    ListCoord[e.ColumnIndex, e.RowIndex].Value = s.ToString("F1");
                }
                catch (Exception) { Console.WriteLine("EndEdit"); }
            }
        }

        private void ListCoord_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if ( canDraw == true ) {
                DrawOnDesk(null);
            }
        }

        private void ListCoord_SelectionChanged(object sender, EventArgs e)
        {
            int index = 0;
            if ( ListCoord.CurrentCell != null )
                index = ListCoord.CurrentCell.ColumnIndex;

            if (index >= 3)
            {
                if (ListCoord.SelectedCells.Count == 1)
                {
                    ListCoord.SelectedCells[0].Selected = false;
                }
            }
        }

        private void ListCoord_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if ( e.ColumnIndex == 0 )
            {
                if ( e.RowIndex == 0 )
                {
                    ListCoord.EndEdit();
                    return;
                }
                bool flag = Convert.ToBoolean(ListCoord[e.ColumnIndex, e.RowIndex].Value);
                ListCoord[e.ColumnIndex, e.RowIndex].Value = !flag;
            }

            if (e.ColumnIndex == 3)
            {
                List<myPoint> list = getList();
                if (list.Count > 0 && list.Count < 128 && list.Count > e.RowIndex)
                {
                    bool flag = false;
                    if (e.RowIndex == 0)
                        flag = true;

                    list.Insert(e.RowIndex, new myPoint(0, 0, flag));
                    canDraw = false;
                    for (int i = 0; i < list.Count; ++i)
                    {
                        ListCoord[0, i].Value = list[i].isBegin;
                        ListCoord[1, i].Value = list[i].x.ToString("F1");
                        ListCoord[2, i].Value = list[i].y.ToString("F1");
                    }
                    canDraw = true;
                    ListCoord.CurrentCell = ListCoord[1, e.RowIndex];
                }
            }
            if (e.ColumnIndex == 4)
            {
                List<myPoint> list = getList();
                if (list.Count > 0 && list.Count < 128 && list.Count > e.RowIndex)
                {
                    canDraw = false;
                    initDataGrid();
                    list.RemoveAt(e.RowIndex);
                    if ( list.Count > 0 )
                    {
                        list.Insert(0, new myPoint(list[0].x, list[0].y, true));
                        list.RemoveAt(1);
                    }
                    
                    for (int i = 0; i < list.Count; ++i)
                    {
                        ListCoord[0, i].Value = list[i].isBegin;
                        ListCoord[1, i].Value = list[i].x.ToString("F1");
                        ListCoord[2, i].Value = list[i].y.ToString("F1");
                    }
                    canDraw = true;
                    DrawOnDesk(null);
                }
            }
        }

        private void ListCoord_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 1 || e.ColumnIndex == 2)
            {
                int col = ListCoord.CurrentCell.ColumnIndex;
                int row = ListCoord.CurrentCell.RowIndex;
                canDraw = false;
                ListCoord[col, row].Value = null;
                canDraw = true;
            }
        }

        private void ListCoord_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (sender is DataGridView)
            {
                DataGridView grid = (DataGridView)sender;
                if (grid.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly)
                {
                    if (!setNextTabableCell(grid, e.ColumnIndex, e.RowIndex))
                    {
                        setNextTabableControl();
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("this method can only be applied to controls of type DataGridView");
            }
        }

        private void Load_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory =  Environment.GetFolderPath(Environment.SpecialFolder.MyDoc‌​uments);
            openFileDialog1.Filter = "draw files (*.draw)|*.draw";
            openFileDialog1.Title = "Завантажити координати малюноку";
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                List<myPoint> points = null;
                string s = string.Empty;
                string s1 = string.Empty;
                bool fYes = false;
                BinaryFormatter bf = null;
                FileStream fs = null;
                try
                {
                    Console.WriteLine(openFileDialog1.FileName);
                    if (File.Exists(openFileDialog1.FileName))
                    {
                        fs = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read);
                        bf = new BinaryFormatter();
                        points = bf.Deserialize(fs) as List<myPoint>;
                        fYes = true;
                        filename = openFileDialog1.FileName;
                        filename = filename.Remove(filename.Length - 5);
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("Не вдалося завантажити координати малюноку!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                    if (bf != null)
                        bf = null;
                }
                if (fYes)
                {
                    ListCoord.Rows.Clear();
                    canDraw = false;
                    for ( int i = 0;i<MAX_ROW;++i )
                    {
                        ListCoord.Rows.Add();
                        if ( i < points.Count)
                        {
                            ListCoord[0, i].Value = points[i].isBegin;
                            ListCoord[1, i].Value = points[i].x.ToString("F1");
                            ListCoord[2, i].Value = points[i].y.ToString("F1");
                        }
                    }
                    canDraw = true;
                    DrawOnDesk(null);
                }
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            if ( filename == null )
            {
                saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDoc‌​uments);
            }else
            {
                saveFileDialog1.InitialDirectory = Path.GetDirectoryName(filename);
                saveFileDialog1.FileName = Path.GetFileName(filename + ".draw");

            }
            saveFileDialog1.Filter = "draw files (*.draw)|*.draw";
            saveFileDialog1.Title = "Зберегти координати малюнку";
            saveFileDialog1.RestoreDirectory = true;
            if ( saveFileDialog1.ShowDialog() == DialogResult.OK )
            {
                List<myPoint> points = null;
                try
                {
                    points = getList();
                    if ( points.Count == 0 )
                    {
                        throw new Exception("Нема що зберігати!");
                    }
                    BinaryFormatter bf = null;
                    string path = saveFileDialog1.FileName;
                    using (FileStream fs = File.Create(path))
                    {
                        try
                        {
                            bf = new BinaryFormatter();
                            bf.Serialize(fs, points);
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Не вдалось зберегти координати малюнку!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            if ( fs != null ) { fs.Close(); }
                            if ( bf != null ) { bf = null; }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        
        private void Clear_Click(object sender, EventArgs e)
        {
            initDataGrid();
            DrawOnDesk(null);
        }

        private void Info_Click(object sender, EventArgs e)
        {
            Help secondForm = new Help();
            secondForm.ShowInTaskbar = false;
            secondForm.StartPosition = FormStartPosition.CenterParent;
            secondForm.Show(this);
        }

        private void saveAsPic_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            if (filename == null)
            {
                saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDoc‌​uments);
            }
            else
            {
                saveFileDialog1.InitialDirectory = Path.GetDirectoryName(filename);
                saveFileDialog1.FileName = Path.GetFileName(filename + ".png");
                

            }
            saveFileDialog1.Filter = "picture files (*.png)|*.png";
            saveFileDialog1.Title = "Зберегти малюнок";
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                List<myPoint> points = null;
                try
                {
                    points = getList();
                    if (points.Count == 0)
                    {
                        throw new Exception("Нема що зберігати!");
                    }
                    Bitmap picture = new Bitmap(DrawArea.Size.Width, DrawArea.Height, PixelFormat.Format32bppPArgb);
                    clear_Table(picture);
                    DrawOnDesk(picture);
                    picture.Save(saveFileDialog1.FileName, ImageFormat.Png);
                    if ( filename == null)
                    {
                        filename = saveFileDialog1.FileName;
                        filename = filename.Remove(filename.Length - 4);
                    }
                    betterSave();
                    isChange = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void showXY_CheckedChanged(object sender, EventArgs e)
        {
            DrawOnDesk(null);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ( isChange == true && getList().Count != 0 )
            {
                betterSave();
            }
        }

        private bool setNextTabableCell(DataGridView grid, int nextColumn, int nextRow)
        {
            do
            {
                if (nextColumn == grid.Columns.Count - 1)
                {
                    nextColumn = 0;
                    if (nextRow == grid.Rows.Count - 1)
                    {
                        nextRow = 0;
                    }
                    else
                    {
                        nextRow = Math.Min(grid.Rows.Count - 1, nextRow + 1);
                    }
                }
                else
                {
                    nextColumn = Math.Min(grid.Columns.Count - 1, nextColumn + 1);
                }
            }
            while (grid.Rows[nextRow].Cells[nextColumn].OwningColumn.AutoSizeMode == DataGridViewAutoSizeColumnMode.NotSet ||
                        grid.Rows[nextRow].Cells[nextColumn].Visible == false);
            
            SetColumnAndRowOnGrid method = new SetColumnAndRowOnGrid(setGridCell);
            grid.BeginInvoke(method, grid, nextColumn, nextRow);
            return true;
        }

        private void setGridCell(DataGridView grid, int columnIndex, int rowIndex)
        {
            grid.CurrentCell = grid.Rows[rowIndex].Cells[columnIndex];
            grid.CurrentCell.Selected = true;
        }

        private void setNextTabableControl()
        {
            this.SelectNextControl(this, true, true, true, true);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F8)
            {
                Clear.PerformClick();
            }
            if (keyData == Keys.F2)
            {
                Save.PerformClick();
            }
            if (keyData == Keys.F3)
            {
                Load.PerformClick();
            }
            if (keyData == Keys.F1)
            {
                Info.PerformClick();
            }
            if (keyData == Keys.S)
            {
                saveAsPic.PerformClick();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private delegate void SetColumnAndRowOnGrid(DataGridView grid, int i, int o);
    }
}
